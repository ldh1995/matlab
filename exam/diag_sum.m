function s=diag_sum(A)
A=input('A=');
[m,n]=size(A);
h=min(m,n);
i=(h+1)/2;
a=A';
if mod(h,2)==0
s=sum(diag(A))+sum(diag(a));
else
s=sum(diag(A))+sum(diag(a))-A(i,i);
end
end

